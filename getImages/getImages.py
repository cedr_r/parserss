from bs4 import BeautifulSoup
import csv
import os
import requests
import urllib.request
import shutil

def download_image(image,media):
    response = requests.get(image[0], stream=True)
    realname = ''.join(e for e in image[1] if e.isalnum())
    
    file = open("images/" + media + "/{}.jpg".format(realname), 'wb')
    
    response.raw.decode_content = True
    shutil.copyfileobj(response.raw, file)
    del response



fullData =[]

res=[]

with open("data/mergeData.csv", newline='') as f:
    reader = csv.reader(f)
    data = list(reader)
    fullData.extend(data)

for i in fullData:
    res.append([i[0],i[3]])



for s in res:

    if s[0] == 'Le Monde.fr - Actualités et Infos en France et dans le monde':
        url = s[1]
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        aas = soup.find_all("figure", class_='article__media')
        image_info = []

        for a in aas:
            image_tag = a.findChildren("img")
            image_info.append((image_tag[0]["src"], image_tag[0]["alt"]))

        for i in range(0, len(image_info)):
            download_image(image_info[i],"lemonde")



    if s[0] == '20Minutes - Une':
        url = s[1]
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        aas = soup.find_all("img", id='main-illustration')
        image_info = []

        for a in aas:
            image_info.append((aas[0]["src"], aas[0]["alt"]))

        for i in range(0, len(image_info)):
            download_image(image_info[i],"20min")


    if s[0] == 'Franceinfo - Les Titres':
        url = s[1]
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        aas = soup.find_all("div", class_='c-cover')
        image_info = []

        for a in aas:
            image_tag = a.findChildren("img")
            image_info.append((image_tag[0]["src"], image_tag[0]["alt"]))

        for i in range(0, len(image_info)):
            download_image(image_info[i],"franceinfo")

 


