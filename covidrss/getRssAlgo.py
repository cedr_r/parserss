import feedparser
from slugify import slugify
from datetime import date
import csv

#liste des mots clés
keyword = ["covid"
    ]

# liste des flux RSS
listRSSFEED = ["https://www.lemonde.fr/rss/une.xml",
    "https://www.monde-diplomatique.fr/recents.xml",
    "https://www.20minutes.fr/feeds/rss-une.xml",
    "https://www.francetvinfo.fr/titres.rss",
    "https://www.liberation.fr/arc/outboundfeeds/rss/category/checknews/?outputType=xml",
    "https://www.liberation.fr/arc/outboundfeeds/rss/?outputType=xml",
    "https://www.lefigaro.fr/rss/figaro_actualites-a-la-une.xml",
    "https://www.lepoint.fr/24h-infos/rss.xml",
    "https://www.mediapart.fr/articles/feed",
    "https://www.latribune.fr/feed.xml",
    "https://www.humanite.fr/rss/actu.rss",
    "https://www.la-croix.com/RSS/UNIVERS_ALL",
    "https://www.lexpress.fr/rss/alaune.xml",
    "https://www.nouvelobs.com/a-la-une/rss.xml",
    "https://cdn1-parismatch.lanmedia.fr/var/exports/rss/rss-actu.xml",
    "https://www.marianne.net/rss.xml",
    "https://factuel.afp.com/rss.xml",
    "https://www.courrierinternational.com/feed/all/rss.xml",
    "https://lentreprise.lexpress.fr/rss/actualite.xml",
    "https://www.francesoir.fr/rss.xml",

    ]




#La date
today = date.today()

#Ce qui va s'imprimer
printThisTOday = []

for l in listRSSFEED :

    try :
            
        NewsFeed = feedparser.parse(l)
        mediaName = NewsFeed.channel.title

        for i in range(len(NewsFeed.entries)) : 

            entry = NewsFeed.entries[i]
            doesItMatch = slugify(entry.title)

            for v in keyword : 

                if v in doesItMatch : 

                    printThisTOday.append([mediaName,v,entry.title,entry.link,entry.updated,entry.description])

    except:
        print ("chien")
        pass


with open("data/" + str(today)+".csv", 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(printThisTOday)
    
print ("done")
