import csv
import os

path = "data/"


fullData =[]
res = []

for g in os.listdir(path):

    with open(path + str(g), newline='') as f:
        reader = csv.reader(f)
        data = list(reader)
        fullData.extend(data)

for i in fullData:
    if i not in res:
        res.append(i)

with open("mergeData/mergeData.csv", 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(res)
    
print ("done")