import csv
import os

path = "mergeData/"
pathSave = "wordTri/"


fullData =[]

for g in os.listdir(path):

    with open(path + str(g), newline='') as f:
        reader = csv.reader(f)
        data = list(reader)
        fullData.extend(data)

for i in range(len(fullData)) :
    print (fullData[i][1])

    with open(pathSave + fullData[i][1] + ".csv", 'a') as file:
        writer = csv.writer(file)
        writer.writerow(fullData[i])
